# Covid19-analysis
This repository attempts to forecast infections and fatalities in the world-wide COVID-19 pandemic. Data from: https://www.kaggle.com/c/covid19-global-forecasting-week-2

## Baseline model
This model was trained and validated on all data before 25-03-2020 in a sliding-window fashion. Predictions are made from 25-03-2020 on (Kaggle forecasting week 2). 
The model is trained to predict both infections and fatalities at the same time, so the model is a multivariate multiple regression model.
The time-series from Kaggle is appended with the following additional data per country:
1. Proportion of world population 
2. Age distribution

Current Kaggle score:
1. Ridge regression: 
    * Leaderboard position: 287 / 511
    * Root Log Mean Squared Error (RLMSE): 0.76961

The following plots show example predictions versus actual numbers in the Netherlands:
* Ridge regression (Regularization parameter = 2.0)

![](images/infections_ridge_nl.png)

![](images/fatalities_ridge_nl.png)

The model could be improved by gathering more additonal country data or using different models.

## interpretability
The following plots show the top 10 highest coefficients and top 10 lowest coefficients (both for infections, not fatalities) of the ridge regression model, respectively.

* Top 10 highest:
![](images/coefs_ridge_highest.png)
* Top 10 lowest:
![](images/coefs_ridge_lowest.png)

According to the coefficients, the 'infections_day_62' feature is assigned a large value, as it is the number of infections of the newest day, which seems very relevant for 
predicting the infections of the following day. Also, 'Age_0_to_14' seems important. More kids = more people playing outside = more infections?

Looking at the negative coefficients, the '% of worldpopulation' and 'Age_15_to_64' are included in the top 10 negative coefficients, which might be different than one may
expect. There are three age distribution groups in the data: Age_0_to_14, Age_15_to_64 and Age_over_64. If Age_15_to_64 is higher, this means that the other two must be lower,
as these three variables sum to 1. This makes the 'Age_0_to_14' lower which, as we saw before, has a positive effect on the number of infections. Maybe 'Age_over_64' 
also has a positive effect on the number of infections?

'% of worldpopulation' being in the top 10 negative coefficients might be due to the fact that certain countries that are not that large still have a lot of infections
(e.g. Spain and Italy). 
